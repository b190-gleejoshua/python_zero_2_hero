index_count = 0

for letter in 'abcdef':
    print("At index {}, the letter is {}".format(index_count, letter))
    index_count += 1
# enumerate function

word = 'abcde'

for index, letter in enumerate(word):
    print(index)

mylist1 = [1, 2, 3]
mylist2 = ['a', 'b', 'c']

for number, letter in zip(mylist1, mylist2):
    print(letter)